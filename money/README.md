# Money Service

Build the project first by installing dependencies using
```$xslt
./gradlew build # for Linux and Mac
gradle build # for Windows
```

Run the application using
```$xslt
./gradlew run # for Linux and Mac
gradlew run # for Windows
```

Note that the application runs from the `MoneyBoot` class.