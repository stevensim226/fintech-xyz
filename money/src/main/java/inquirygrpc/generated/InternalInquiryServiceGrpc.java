package inquirygrpc.generated;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.46.0)",
    comments = "Source: moneyinquiry.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class InternalInquiryServiceGrpc {

  private InternalInquiryServiceGrpc() {}

  public static final String SERVICE_NAME = "InternalInquiryService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<Moneyinquiry.InquiryRequest,
      Moneyinquiry.InquiryResponse> getGetSaldoFromRekNumberMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetSaldoFromRekNumber",
      requestType = Moneyinquiry.InquiryRequest.class,
      responseType = Moneyinquiry.InquiryResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<Moneyinquiry.InquiryRequest,
      Moneyinquiry.InquiryResponse> getGetSaldoFromRekNumberMethod() {
    io.grpc.MethodDescriptor<Moneyinquiry.InquiryRequest, Moneyinquiry.InquiryResponse> getGetSaldoFromRekNumberMethod;
    if ((getGetSaldoFromRekNumberMethod = InternalInquiryServiceGrpc.getGetSaldoFromRekNumberMethod) == null) {
      synchronized (InternalInquiryServiceGrpc.class) {
        if ((getGetSaldoFromRekNumberMethod = InternalInquiryServiceGrpc.getGetSaldoFromRekNumberMethod) == null) {
          InternalInquiryServiceGrpc.getGetSaldoFromRekNumberMethod = getGetSaldoFromRekNumberMethod =
              io.grpc.MethodDescriptor.<Moneyinquiry.InquiryRequest, Moneyinquiry.InquiryResponse>newBuilder()
                  .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
                  .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetSaldoFromRekNumber"))
                  .setSampledToLocalTracing(true)
                  .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                      Moneyinquiry.InquiryRequest.getDefaultInstance()))
                  .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                      Moneyinquiry.InquiryResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new InternalInquiryServiceMethodDescriptorSupplier("GetSaldoFromRekNumber"))
                  .build();
        }
      }
    }
    return getGetSaldoFromRekNumberMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static InternalInquiryServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<InternalInquiryServiceStub> factory =
        new io.grpc.stub.AbstractStub.StubFactory<InternalInquiryServiceStub>() {
          @java.lang.Override
          public InternalInquiryServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new InternalInquiryServiceStub(channel, callOptions);
          }
        };
    return InternalInquiryServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static InternalInquiryServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<InternalInquiryServiceBlockingStub> factory =
        new io.grpc.stub.AbstractStub.StubFactory<InternalInquiryServiceBlockingStub>() {
          @java.lang.Override
          public InternalInquiryServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new InternalInquiryServiceBlockingStub(channel, callOptions);
          }
        };
    return InternalInquiryServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static InternalInquiryServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<InternalInquiryServiceFutureStub> factory =
        new io.grpc.stub.AbstractStub.StubFactory<InternalInquiryServiceFutureStub>() {
          @java.lang.Override
          public InternalInquiryServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new InternalInquiryServiceFutureStub(channel, callOptions);
          }
        };
    return InternalInquiryServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class InternalInquiryServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void getSaldoFromRekNumber(Moneyinquiry.InquiryRequest request,
                                      io.grpc.stub.StreamObserver<Moneyinquiry.InquiryResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetSaldoFromRekNumberMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
              getGetSaldoFromRekNumberMethod(),
              io.grpc.stub.ServerCalls.asyncUnaryCall(
                  new MethodHandlers<
                      Moneyinquiry.InquiryRequest,
                      Moneyinquiry.InquiryResponse>(
                      this, METHODID_GET_SALDO_FROM_REK_NUMBER)))
          .build();
    }
  }

  /**
   */
  public static final class InternalInquiryServiceStub extends io.grpc.stub.AbstractAsyncStub<InternalInquiryServiceStub> {
    private InternalInquiryServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected InternalInquiryServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new InternalInquiryServiceStub(channel, callOptions);
    }

    /**
     */
    public void getSaldoFromRekNumber(Moneyinquiry.InquiryRequest request,
                                      io.grpc.stub.StreamObserver<Moneyinquiry.InquiryResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetSaldoFromRekNumberMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class InternalInquiryServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<InternalInquiryServiceBlockingStub> {
    private InternalInquiryServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected InternalInquiryServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new InternalInquiryServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public Moneyinquiry.InquiryResponse getSaldoFromRekNumber(Moneyinquiry.InquiryRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetSaldoFromRekNumberMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class InternalInquiryServiceFutureStub extends io.grpc.stub.AbstractFutureStub<InternalInquiryServiceFutureStub> {
    private InternalInquiryServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected InternalInquiryServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new InternalInquiryServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<Moneyinquiry.InquiryResponse> getSaldoFromRekNumber(
        Moneyinquiry.InquiryRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetSaldoFromRekNumberMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_SALDO_FROM_REK_NUMBER = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final InternalInquiryServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(InternalInquiryServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_SALDO_FROM_REK_NUMBER:
          serviceImpl.getSaldoFromRekNumber((Moneyinquiry.InquiryRequest) request,
              (io.grpc.stub.StreamObserver<Moneyinquiry.InquiryResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class InternalInquiryServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    InternalInquiryServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return Moneyinquiry.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("InternalInquiryService");
    }
  }

  private static final class InternalInquiryServiceFileDescriptorSupplier
      extends InternalInquiryServiceBaseDescriptorSupplier {
    InternalInquiryServiceFileDescriptorSupplier() {}
  }

  private static final class InternalInquiryServiceMethodDescriptorSupplier
      extends InternalInquiryServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    InternalInquiryServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (InternalInquiryServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new InternalInquiryServiceFileDescriptorSupplier())
              .addMethod(getGetSaldoFromRekNumberMethod())
              .build();
        }
      }
    }
    return result;
  }
}
