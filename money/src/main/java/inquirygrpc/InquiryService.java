package inquirygrpc;

import inquirygrpc.generated.InternalInquiryServiceGrpc;
import inquirygrpc.generated.Moneyinquiry;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class InquiryService {

    private final InternalInquiryServiceGrpc.InternalInquiryServiceBlockingStub blockingStub;


    public InquiryService(String inquiryGrpcHost) {
        ManagedChannel channel = ManagedChannelBuilder.forTarget(inquiryGrpcHost)
            .usePlaintext()
            .build();
        this.blockingStub = InternalInquiryServiceGrpc.newBlockingStub(channel);
    }

    public int getSaldoFromRekening(String rekeningNumber) {
        Moneyinquiry.InquiryRequest request = Moneyinquiry.InquiryRequest
            .newBuilder()
            .setRekeningNumber(rekeningNumber)
            .build();

        return blockingStub.getSaldoFromRekNumber(request).getResult();
    }
}
