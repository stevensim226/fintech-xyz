package db;

import java.util.List;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

public class RekeningDao {

    private Sql2o sql2o;

    public RekeningDao(String jdbcUrl, String dbUser, String dbPassword) {
        this.sql2o = new Sql2o(jdbcUrl, dbUser, dbPassword);
    }

    public void modifyRekeningBalance(String rekeningNo, int modifyAmount) {
        String sql = "UPDATE rekening " +
            "SET saldo = saldo + :modifyAmount " +
            "WHERE no_rekening = :rekeningNo";

        try (Connection con = sql2o.beginTransaction()) {
            con.createQuery(sql)
                .addParameter("modifyAmount", modifyAmount)
                .addParameter("rekeningNo", rekeningNo)
                .executeUpdate();
            con.commit();
        }
    }

    public void updateTransactionStatus(String transferId, String status) {
        String success_transaction = "UPDATE transaksi SET status = :status WHERE id_transaksi = :transferId";

        try (Connection con = sql2o.beginTransaction()){
            con.createQuery(success_transaction)
                    .addParameter("status", status)
                    .addParameter("transferId", transferId)
                    .executeUpdate();
            con.commit();
        }
    }
}
