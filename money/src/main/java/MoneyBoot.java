import db.RekeningDao;
import inquirygrpc.InquiryService;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class MoneyBoot {
    public static void main(String args[]) throws IOException, TimeoutException {

        RekeningDao rekeningDao = new RekeningDao(
                System.getenv("JDBC_URL"),
                System.getenv("DB_USER"),
                System.getenv("DB_PASS")
        );

        // TODO: Change this to real server!
        InquiryService inquiryService = new InquiryService("35.192.154.100:50051");

        QueueListener queueListener = new QueueListener(
                System.getenv("RABBITMQ_HOST"), 5672,
                System.getenv("RABBITMQ_USER"),
                System.getenv("RABBITMQ_PASS"),
                rekeningDao,
                inquiryService);

        System.out.println("System ready to serve transactions");

        //rekeningDao.modifyRekeningBalance("4583078166", 200000);
        //rekeningDao.modifyRekeningBalance("4583078166", -15000);
        //System.out.println(inquiryService.getSaldoFromRekening("131311241"));
        //System.out.println(inquiryService.getSaldoFromRekening("13131232321241"));
    }
}
