import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import db.RekeningDao;
import inquirygrpc.InquiryService;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import org.json.JSONObject;
import logger.XyzLogger;

public class QueueListener {

    private Connection connection;
    private Channel channel;
    private RekeningDao rekeningDao;
    private InquiryService inquiryService;
    final private String DELAYED_TRANSFER_QUEUE = "delayed_transfer_queue";
    final private String INSTANT_TRANSFER_QUEUE = "instant_transfer_queue";
    final private String TOPUP_QUEUE = "topup_queue";
    final private String PAYMENT_LINK_QUEUE = "payment_link_queue";

    QueueListener(String host, int port, String username, String password, RekeningDao rekeningDao, InquiryService inquiryService)
        throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setPort(port);
        factory.setUsername(username);
        factory.setPassword(password);

        this.connection = factory.newConnection();
        this.channel = connection.createChannel();

        this.rekeningDao = rekeningDao;
        this.inquiryService = inquiryService;

        // TODO: Call subscribe functions here
        subscribeDelayedTransfer();
        subscribeInstantTransfer();
        subscribePaymentLink();
        subscribeTopup();
        //XyzLogger.info("Money service boot finished successfully!");
    }

    private void subscribeDelayedTransfer() throws IOException {
        System.out.println("Subscribing to delayed transfer queue...");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            //XyzLogger.info(" [x] Delayed Received: " + message);
            performTransfer(message);
        };
        channel.basicConsume(DELAYED_TRANSFER_QUEUE, true, deliverCallback, consumerTag -> {});
        //XyzLogger.info("Finished subscribing...");
    }

    private void subscribeInstantTransfer() throws IOException {
        System.out.println("Subscribing to instant transfer queue...");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            //XyzLogger.info(" [x] Instant Received: " + message);

            performTransfer(message);
        };
        channel.basicConsume(INSTANT_TRANSFER_QUEUE, true, deliverCallback, consumerTag -> {});
        //XyzLogger.info("Finish subscribing...");
    }

    private void performTransfer(String message) throws IOException {
        JSONObject parsed_details = new JSONObject(message);
        int decreased_nominal = parsed_details.getInt("nominal")*-1;
        int nominal = parsed_details.getInt("nominal");
        String sender_acc = parsed_details.getString("sender_account");
        if (balanceCheck(sender_acc, nominal)) {
            try {
                rekeningDao.modifyRekeningBalance(sender_acc, decreased_nominal); //sender account
                rekeningDao.modifyRekeningBalance(parsed_details.getString("target_account"), nominal); //receiver account
                rekeningDao.updateTransactionStatus(parsed_details.getString("transfer_id"), "SUCCESS"); //update success status
                //XyzLogger.info(" [x] Transfer Success");
            } catch (Exception e){
                rekeningDao.updateTransactionStatus(parsed_details.getString("transfer_id"), "FAILED"); //update failed status
                //XyzLogger.info(" [x] Transfer Failed: An error occurred ");
            }
        } else {
            //XyzLogger.info(" [x] Transfer Failed: Balance is not enough ");
        }
    }

    private void subscribeTopup() throws IOException {
        System.out.println("Subscribing to topup queue...");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");

            performTopup(message);
        };
        channel.basicConsume(TOPUP_QUEUE, true, deliverCallback, consumerTag -> {});
    }

    private void performTopup(String message) throws IOException {
        JSONObject parsedDetails = new JSONObject(message);
        int topupAmount = parsedDetails.getInt("amount");
        String rekeningNumber = parsedDetails.getString("no_rekening");
        
        try {
            rekeningDao.modifyRekeningBalance(rekeningNumber, topupAmount); //topup account
            rekeningDao.updateTransactionStatus(parsedDetails.getString("transfer_id"), "SUCCESS"); //update success status
        } catch (Exception e){
            rekeningDao.updateTransactionStatus(parsedDetails.getString("transfer_id"), "FAILED"); //update failed status
        }
    }

    private boolean balanceCheck(String account, int nominal) {
        int balance = inquiryService.getSaldoFromRekening(account);
        return (nominal <= balance);
    }

    // TODO: Create more subscribe functions here...

    private void subscribePaymentLink() throws IOException {
        System.out.println("Subscribing to payment link queue...");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            //XyzLogger.info(" [x] Instant Received: " + message);

            performPayment(message);
        };
        channel.basicConsume(PAYMENT_LINK_QUEUE, true, deliverCallback, consumerTag -> {});
        //XyzLogger.info("Finish subscribing...");
    }

    private void performPayment(String message) throws IOException {
        JSONObject parsed_details = new JSONObject(message);
        String id_transaksi = parsed_details.getString("id_transaksi");
        int nominal = parsed_details.getInt("nominal");
        int decreased_nominal = parsed_details.getInt("nominal")*-1;
        String no_rekening_pengirim = parsed_details.getString("no_rekening_pengirim");
        String no_rekening_penerima = parsed_details.getString("no_rekening_penerima");
        if (balanceCheck(no_rekening_pengirim, nominal)) {
            try {
                rekeningDao.modifyRekeningBalance(no_rekening_pengirim, decreased_nominal); //sender account
                rekeningDao.modifyRekeningBalance(no_rekening_penerima, nominal); //receiver account
                rekeningDao.updateTransactionStatus(id_transaksi, "SUCCESS"); //update success status
                //XyzLogger.info(" [x] Transfer Success");
            } catch (Exception e){
                rekeningDao.updateTransactionStatus(id_transaksi, "FAILED"); //update failed status
                //XyzLogger.info(" [x] Transfer Failed: An error occurred ");
            }
        } else {
            //XyzLogger.info(" [x] Transfer Failed: Balance is not enough ");
        }
    }
}
