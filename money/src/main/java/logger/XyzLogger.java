package logger;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.IndexRequest;
import co.elastic.clients.json.JsonData;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;

public class XyzLogger {

    private static RestClient restClient = RestClient.builder(
        new HttpHost("34.124.241.9", 9200)).build();

    private static ElasticsearchTransport transport = new RestClientTransport(
        restClient, new JacksonJsonpMapper());

    private static ElasticsearchClient es = new ElasticsearchClient(transport);

    private static final String INDEX_NAME = "fintechxyz-app";

    private static String buildJsonMessage(String message, String level) {
        return "{\"context\" : \"money\",  \"level\" : \"" + level +  "\", " +
            "\"message\" : \"" + message + "\" " +
            "}";
    }

    public static void warning(String message) throws IOException {
        Reader input = new StringReader(buildJsonMessage(message, "WARNING"));

        IndexRequest<JsonData> request = IndexRequest.of(i -> i
            .index(INDEX_NAME)
            .withJson(input)
        );

        es.index(request);
    }

    public static void info(String message) throws IOException {
        Reader input = new StringReader(buildJsonMessage(message, "INFO"));

        IndexRequest<JsonData> request = IndexRequest.of(i -> i
            .index(INDEX_NAME)
            .withJson(input)
        );

        es.index(request);
    }

    public static void error(String message) throws IOException {
        Reader input = new StringReader(buildJsonMessage(message, "ERROR"));

        IndexRequest<JsonData> request = IndexRequest.of(i -> i
            .index(INDEX_NAME)
            .withJson(input)
        );

        es.index(request);
    }

}
