from datetime import datetime
from pydantic import BaseModel
from typing import List

class BaseResponse(BaseModel):
    success: bool

class MessageResponse(BaseResponse):
    message: str

class SaldoResponse(BaseResponse):
    username: str
    saldo: str
    no_rekening: str

class Transaksi(BaseModel):
    id_transaksi: str
    keterangan: str
    waktu: datetime
    nominal: str
    status: str
    tipe: str

class RiwayatTransaksiResponse(BaseResponse):
    username: str
    riwayat: List[Transaksi]

