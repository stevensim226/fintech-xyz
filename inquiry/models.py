from decouple import config
import psycopg2
import sys
from pathlib import Path
import os
sys.path.append(os.path.abspath(os.path.join(Path(__file__).parent, '../common')))
import xyz_logger
import requests
import datetime

APP_NAME = "inquiry"
DB_CONN = psycopg2.connect(
    host=config("DB_HOST"),
    database=config("DB_NAME"),
    user=config("DB_USER"),
    password=config("DB_PASS")
)

DB_CURSOR = DB_CONN.cursor()

# Key: no_rekening
# Value: (riwayat_transaksi, date_created)
RIWAYAT_TRANSAKSI_LIST_CACHE = {}

def get_username_from_token(access_token):
  url = 'http://34.143.212.167:5000/whoami'
  headers = {"authentication": "Bearer " + access_token}
  r = requests.get(url, headers=headers)
  response = r.json()
  return response['username'] if response['success'] else None
  
def get_saldo_and_no_rekening_from_username(username):
  query = f"SELECT username, saldo, no_rekening FROM rekening WHERE username = '{username}'"
  DB_CURSOR.execute(query)
  fetch_res = DB_CURSOR.fetchone()
  saldo = fetch_res[1]
  no_rekening = fetch_res[2]
  print(fetch_res)
  xyz_logger.info(f"Get saldo and no rekening has done for {username}", APP_NAME)
  return fetch_res if fetch_res is None else (saldo, no_rekening)

def get_riwayat_transaksi_from_no_rekening(no_rekening, force_update = "false"):
  riwayat_transaksi_cache = RIWAYAT_TRANSAKSI_LIST_CACHE.get(no_rekening)
  if riwayat_transaksi_cache and force_update == "false":
    time_diff = (datetime.datetime.now() - riwayat_transaksi_cache[1]).seconds / 60.0
    if time_diff < 5:
      xyz_logger.info(f"Get riwayat transaksi from cache has done for no rekening {no_rekening}", APP_NAME)
      print(f"cache used for no_rekening {no_rekening}")
      return riwayat_transaksi_cache[0]

  query = f"SELECT * FROM transaksi WHERE no_rekening_pengirim = '{no_rekening}' OR no_rekening_penerima = '{no_rekening}'"
  DB_CURSOR.execute(query)
  fetch_res = DB_CURSOR.fetchall()
  xyz_logger.info(f"Get riwayat transaksi has done for no rekening {no_rekening}", APP_NAME)
  RIWAYAT_TRANSAKSI_LIST_CACHE[no_rekening] = (fetch_res, datetime.datetime.now())
  return fetch_res

def get_saldo_from_no_rekening(no_rekening):
  query = f"SELECT saldo FROM rekening WHERE no_rekening = '{no_rekening}'"
  DB_CURSOR.execute(query)
  fetch_res = DB_CURSOR.fetchone()
  saldo = fetch_res[0]
  xyz_logger.info(f"Get saldo has done for no rekening {no_rekening}", APP_NAME)
  return fetch_res if fetch_res is None else (saldo) 