from fastapi import FastAPI, Header
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
import datetime
from grpc_fintechxyz import main
from threading import Thread

from models import get_username_from_token, get_saldo_and_no_rekening_from_username, get_riwayat_transaksi_from_no_rekening
from response_models import MessageResponse, SaldoResponse, RiwayatTransaksiResponse

AUTH_HEADER = Header(None, description="Bearer token that contains the access token. Must be in 'Bearer ...' format")

app = FastAPI()

app.add_middleware(CORSMiddleware, 
    allow_origins = ["*"],
    allow_methods=["*"],
    allow_headers=["*"]
)

@app.get("/cek-saldo", response_model=SaldoResponse, responses={401: {"model": MessageResponse}})
async def cek_saldo(authentication = AUTH_HEADER):
    if authentication is None:
        return JSONResponse(status_code=401, content={
            "success": False,
            "message": "Header not valid!"
        })
        
    username = get_username_from_token(authentication[7:])

    if username is None:
        return JSONResponse(status_code=401, content={
            "success": False,
            "message": "Token not valid!"
        })
    
    saldo, no_rekening = get_saldo_and_no_rekening_from_username(username)
    return JSONResponse(status_code=200, content={
        "success": True,
        "username": username,
        "saldo": saldo,
        "no_rekening": no_rekening,
    })

def default(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.isoformat()

@app.get("/riwayat-transaksi", response_model=RiwayatTransaksiResponse, responses={401: {"model": MessageResponse}})
async def riwayat_transaksi(force_update = "false", authentication = AUTH_HEADER):
    if authentication is None:
        return JSONResponse(status_code=401, content={
            "success": False,
            "message": "Header not valid!"
        })

    username = get_username_from_token(authentication[7:])

    if username is None:
        return JSONResponse(status_code=401, content={
            "success": False,
            "message": "Token not valid!"
        })

    saldo, no_rekening = get_saldo_and_no_rekening_from_username(username)
    riwayat_transaksi = get_riwayat_transaksi_from_no_rekening(no_rekening, force_update)
    riwayat = []
    for trx in riwayat_transaksi:
        riwayat.append({
              "id_transaksi": trx[0],
              "waktu": str(trx[1]),
              "nominal": trx[2],
              "status": trx[3],
              "tipe": trx[4],
              "keterangan": trx[5],
        })

    return JSONResponse(status_code=200, content={
        "success": True,
        "username": username,
        "riwayat": riwayat
    })

grpc = Thread(target=main)
grpc.start()