import http from "k6/http";
import { sleep } from "k6";

export let options = {
  insecureSkipTLSVerify: true,
  noConnectionReuse: false,
  stages: [
    { duration: "1m", target: 25 },
    { duration: "30s", target: 25 },
    { duration: "1m", target: 50 },
    { duration: "30s", target: 50 },
    { duration: "1m", target: 75 },
    { duration: "30s", target: 75 },
    { duration: "1m", target: 100 },
    { duration: "30s", target: 100 },
    { duration: "1m", target: 0 },
  ],
};

const API_BASE_URL = "http://35.192.154.100:8000";

export default () => {
  const access_token = "okSplFdmFytNKluOxoBKyyHVqVJRztosEVHDfzGq";
  const header = { headers: { authentication: `Bearer ${access_token}` } };

  http.batch([["GET", `${API_BASE_URL}/cek-saldo`, null, header]]);

  sleep(1);
};
