import grpc
from concurrent import futures
from models import get_saldo_from_no_rekening
from moneyinquiry_pb2_grpc import (InternalInquiryServiceServicer,
	add_InternalInquiryServiceServicer_to_server)
from moneyinquiry_pb2 import InquiryResponse

class InternalInquiryService(InternalInquiryServiceServicer):
  def GetSaldoFromRekNumber(self, request, context):
    no_rekening = request.rekening_number
    print(f"Inquiry requested for {no_rekening}")

    response = InquiryResponse()
    response.result = get_saldo_from_no_rekening(no_rekening)

    return response

def main():
	server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

	add_InternalInquiryServiceServicer_to_server(
		InternalInquiryService(), server
	)

	print('Starting server. Listening on port 50051.')
	server.add_insecure_port('0.0.0.0:50051')
	server.start()

	while True:
		pass
