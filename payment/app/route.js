module.exports = (app) => {
  var router = require("express").Router();
  const payment = require("./controller.js");

  router.post("/create", payment.create);
  router.get("/:code", payment.get);
  router.post("/pay", payment.pay);

  app.use("/payment-link", router);
};