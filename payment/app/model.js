const createPaymentResponseBodySuccess = ({ nominal, link }) => {
  return {
    success: true,
    nominal: nominal,
    link: link
  }
}

const getPaymentResponseBodySuccess = ({ code, nominal }) => {
  return {
    success: true,
    code: code,
    nominal: nominal
  }
}

const payPaymentResponseBodySuccess = (message) => {
  return {
    success: true,
    message: message
  }
}

const responseBodyFailed = (message) => {
  return {
    success: false,
    message: message
  }
}

module.exports = { createPaymentResponseBodySuccess, getPaymentResponseBodySuccess, payPaymentResponseBodySuccess, responseBodyFailed }