const pool = require("./config/db.config.js");
const model = require("./model.js");
const axios = require('axios');
const { v4: uuidv4 } = require("uuid");
const amqp = require('amqplib/callback_api');


const create = (req, res) => {
  // Verify user
  if (!req.get("authentication")) {
    res.status(401).send(model.responseBodyFailed('token is invalid'));
    return;
  }

  if (!req.body.nominal) {
    res.status(400).send(model.responseBodyFailed('bad request'));
    return;
  }

  // Get username from token
  const url = 'http://34.143.212.167:5000/whoami'
  const headers = {"authentication": req.get("authentication")}

  axios.get(url, { headers: headers })
  .then(result => {
    const username = result.data.username

    // Query nomor rekening
    pool.query(`
      SELECT no_rekening FROM rekening WHERE username=$1 
    `, [username], (error, results) => {
      if (error) {
        res.status(500).send(model.responseBodyFailed('internal server error'))
        return;
      }

      const no_rekening = results.rows[0].no_rekening;
      const id_transaksi = uuidv4()

      // Query create payment
      pool.query(`
        INSERT INTO transaksi(id_transaksi, tanggal, nominal, status, tipe, keterangan, no_rekening_penerima) 
        VALUES ($1, now(), $2, 'ON GOING', 'kredit', 'Payment Link', $3)
        RETURNING *
      `, [id_transaksi, req.body.nominal, no_rekening], (error, results) => {
        if (error) {
          res.status(500).send(model.responseBodyFailed('internal server error'))
        }

        res.send(model.createPaymentResponseBodySuccess({
          nominal: results.rows[0].nominal,
          link: results.rows[0].id_transaksi,
        }))
      })
    })
  })
  .catch(error => {
    res.status(400).send(model.responseBodyFailed('token is invalid'))
  });
};


const get = (req, res) => {
  const { code } = req.params;

  // Verify user
  if (!req.get("authentication")) {
    res.status(401).send(model.responseBodyFailed('token is invalid'));
    return;
  }

  if (!code) {
    res.status(400).send(model.responseBodyFailed('bad request'));
    return;
  }

  // Get username from token
  const url = 'http://34.143.212.167:5000/whoami'
  const headers = {"authentication": req.get("authentication")}

  axios.get(url, { headers: headers })
  .then(result => {
    // Query payment link
    pool.query(`
      SELECT id_transaksi, nominal, keterangan, status FROM transaksi 
      WHERE id_transaksi=$1 AND keterangan='Payment Link' AND status='ON GOING'
    `, [code], (error, results) => {
      if (error) {
        res.status(500).send(model.responseBodyFailed('internal server error'))
        return;
      }

      if (results.rowCount === 0) {
        res.status(400).send(model.responseBodyFailed('payment link is unavailable'))
        return;
      }

      res.send(model.getPaymentResponseBodySuccess({
        code: results.rows[0].id_transaksi,
        nominal: results.rows[0].nominal,
      }))
    })
  })
  .catch(error => {
    res.status(500).send(model.responseBodyFailed('token is invalid'))
  });
}


const pay = (req, res) => {
  // Verify user
  if (!req.get("authentication")) {
    res.status(401).send(model.responseBodyFailed('token is invalid'));
    return;
  }

  if (!req.body.code) {
    res.status(400).send(model.responseBodyFailed('bad request'));
    return;
  }

  // Get username from token
  const url = 'http://34.143.212.167:5000/whoami'
  const headers = {"authentication": req.get("authentication")}

  axios.get(url, { headers: headers })
  .then(result => {
    const username = result.data.username

    // Query nomor rekening
    pool.query(`
      SELECT no_rekening FROM rekening WHERE username=$1 
    `, [username], (error, results) => {
      if (error) {
        res.status(500).send(model.responseBodyFailed('internal server error'))
        return;
      }

      const no_rekening = results.rows[0].no_rekening;

      // Query pay payment
      pool.query(`
        UPDATE transaksi SET no_rekening_pengirim=$1 
        WHERE id_transaksi=$2 AND keterangan='Payment Link' AND status='ON GOING'
        RETURNING *
      `, [no_rekening, req.body.code], (error, results) => {
        if (error) {
          res.status(500).send(model.responseBodyFailed('internal server error'))
          return;
        }

        if (results.rowCount === 0) {
          res.status(400).send(model.responseBodyFailed('payment link is unavailable'))
          return;
        }

        // Message queuing
        amqp.connect('amqp://admin:admin@34.124.241.9:5672', function(error0, connection) {
          if (error0) {
            throw error0;
          }
          connection.createChannel(function(error1, channel) {
            if (error1) {
              throw error1;
            }
            const EXCHANGE = "payment_link"
            const QUEUE = "payment_link_queue"
            const msg = JSON.stringify(results.rows[0]);

            channel.assertExchange(EXCHANGE, 'direct', {
              durable: true
            });

            channel.assertQueue(QUEUE, {
              durable: true
            });

            channel.bindQueue(QUEUE, EXCHANGE, EXCHANGE);

            channel.publish(EXCHANGE, EXCHANGE, Buffer.from(msg));
            console.log(" [x] Sent %s", msg);
          });
          
          setTimeout(function() {
            connection.close();
          }, 500);
        });

        res.send(model.payPaymentResponseBodySuccess("Payment berhasil!"))
      })
    })
  })
  .catch(error => {
    res.status(500).send(model.responseBodyFailed('token is invalid'))
  });
}

module.exports = { create, get, pay }