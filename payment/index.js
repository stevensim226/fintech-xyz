const express = require("express");
const app = express();
const cors = require("cors");

const PORT = 8080;

app.use(cors());
// parse requests of content-type - application/json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

require("./app/route.js")(app);

// listen for requests
app.listen(process.env.PORT || PORT, () => {
  console.log(`Payment server is running on port ${PORT}.`);
});