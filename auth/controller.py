from fastapi import FastAPI, Header
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware

from models import login_user, get_username_from_token, register_user
from response_models import MessageResponse, LoginResponse, WhoAmIResponse
from request_models import LoginCredentialsRequest, RegisterUserRequest

AUTH_HEADER = Header(None, description="Bearer token that contains the access token. Must be in 'Bearer ...' format")

app = FastAPI()

app.add_middleware(CORSMiddleware, 
    allow_origins = ["*"],
    allow_methods=["*"],
    allow_headers=["*"]
)

@app.post("/login", response_model=LoginResponse, responses={400: {"model": MessageResponse}})
async def login(request: LoginCredentialsRequest):
    access_token, refresh_token = login_user(request.username, request.password)

    if access_token is None:
        return JSONResponse(status_code=400, content={
            "success": False,
            "message": "Wrong username or password!"
        })

    return JSONResponse(status_code=200, content={
        "success": True,
        "access_token": access_token,
        "refresh_token": refresh_token
    })

@app.post("/register", response_model=MessageResponse, responses={400: {"model": MessageResponse}})
async def register(request: RegisterUserRequest):
    if request.password != request.password2:
        return JSONResponse(status_code=400, content={
            "success": False,
            "message": "Passwords do not match!"
        })
    
    try:
        register_user(request.username, request.password)
        return JSONResponse(status_code=200, content={
            "success": True,
            "message": f"User with the name {request.username} has been succesfully registered, please login"
        })
    except ValueError as err:
        return JSONResponse(status_code=400, content={
            "success": False,
            "message": str(err)
        })

@app.get("/whoami", response_model=WhoAmIResponse, responses={401: {"model": MessageResponse}})
async def whoami(authentication = AUTH_HEADER):
    username = get_username_from_token(authentication[7:]) # Cut off 'Bearer ' head

    if username is None:
        return JSONResponse(status_code=401, content={
            "success": False,
            "message": "Token not valid!"
        })

    return JSONResponse(status_code=200, content={
        "success": True,
        "username": username
    })