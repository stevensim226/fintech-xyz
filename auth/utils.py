import random
import string

def generate_random_token():
	return ''.join(random.choice(string.ascii_letters) for i in range(40))

def generate_random_rekening_no():
	return ''.join(random.choice(string.digits) for i in range(10))