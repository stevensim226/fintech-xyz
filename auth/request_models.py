from pydantic import BaseModel

class LoginCredentialsRequest(BaseModel):
    username: str
    password: str

class RegisterUserRequest(BaseModel):
	username: str
	password: str
	password2: str