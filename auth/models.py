import psycopg2
import sys
from utils import generate_random_token, generate_random_rekening_no
from pathlib import Path
import os
sys.path.append(os.path.abspath(os.path.join(Path(__file__).parent, '../common')))
import xyz_logger

APP_NAME = "auth"
DB_CONN = psycopg2.connect(
    host=os.environ.get("DB_HOST"),
    database=os.environ.get("DB_NAME"),
    user=os.environ.get("DB_USER"),
    password=os.environ.get("DB_PASS"))

DB_CURSOR = DB_CONN.cursor()

def create_new_rekening(username):
	query = f"""INSERT INTO rekening (username, no_rekening, saldo)
		VALUES ('{username}', '{generate_random_rekening_no()}', 0)
	"""
	DB_CURSOR.execute(query)
	DB_CONN.commit()
	xyz_logger.info(f"New rekening for {username} was created", APP_NAME)

def register_user(username, password):
	query = f"SELECT username FROM users WHERE username = '{username}'"
	DB_CURSOR.execute(query)
	fetch_res = DB_CURSOR.fetchone()

	if not fetch_res is None:
		raise ValueError(f"Username with the name {username} already exists!")

	query = f"INSERT INTO USERS (username, password) VALUES ('{username}', '{password}')"
	DB_CURSOR.execute(query)
	DB_CONN.commit()

	xyz_logger.info(f"{username} registered on the app", APP_NAME)
	create_new_rekening(username)

def login_user(username, password):
	query = f"SELECT password FROM users WHERE username = '{username}'"
	DB_CURSOR.execute(query)
	actual_pass_entry = DB_CURSOR.fetchone()

	if not actual_pass_entry is None and actual_pass_entry[0] == password:
		access_token = generate_random_token()
		refresh_token = generate_random_token()

		query = f"""UPDATE users SET access_token = '{access_token}', 
			refresh_token = '{refresh_token}',
			token_expiry = NOW() + '6h'
			WHERE username = '{username}' 
		"""
		DB_CURSOR.execute(query)
		DB_CONN.commit()

		return access_token, refresh_token
	
	xyz_logger.warning(f"Username {username} failed login with wrong password or username" ,APP_NAME)
	return None, None

def get_username_from_token(access_token):
	query = f"""SELECT username FROM users WHERE access_token = '{access_token}'
		AND token_expiry > NOW()
	"""
	DB_CURSOR.execute(query)
	fetch_res = DB_CURSOR.fetchone()
	return fetch_res if fetch_res is None else fetch_res[0]