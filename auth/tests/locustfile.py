import random
from string import ascii_letters
from locust import HttpUser, task, between

VALID_USERS = [(f"user{x}", f"user{x}") for x in range(500)]

def generate_random_string(length):
	return ''.join(random.choice(ascii_letters) for i in range(length))


class AuthMicroserviceTasks(HttpUser):
	wait_time = between(5,10)

	@task(10)
	def login_then_get_whoami(self):
		uname, password = random.choice(VALID_USERS)
		response = self.client.post("/login", json={
			"username": uname,
  			"password": password
		})
		access_token = response.json()["access_token"]

		response = self.client.get("/whoami", headers={
			"Authentication": f"Bearer {access_token}"
		})
		print(response.json()["success"],uname,password)

	@task(1)
	def register_then_login_then_get_whoami(self):
		uname, password = generate_random_string(
			random.randint(8,12)), generate_random_string(
				random.randint(8,12))

		self.client.post("/register", json={
			"username": uname,
  			"password": password,
			"password2": password
		})

		response = self.client.post("/login", json={
			"username": uname,
  			"password": password
		})
		access_token = response.json()["access_token"]

		response = self.client.get("/whoami", headers={
			"Authentication": f"Bearer {access_token}"
		})
