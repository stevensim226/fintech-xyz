from pydantic import BaseModel

class BaseResponse(BaseModel):
    success: bool

class MessageResponse(BaseResponse):
    message: str

class LoginResponse(BaseResponse):
    access_token: str
    refresh_token: str

class WhoAmIResponse(BaseResponse):
    username: str