from django.urls import path
from frontend import views

urlpatterns = [
    path("", views.welcome_page, name="welcome_page"),
    path("login", views.login_page, name="login_page"),
    path("logout", views.logout_page, name="logout_page"),
    path("register", views.register_page, name="register_page"),
    path("inquiry", views.inquiry_page, name="inquiry_page"),
    path("payment-link", views.create_payment_link_page,
         name="create_payment_link_page"),
    path("payment-link/<slug:code>",
         views.payment_link_page, name="payment_link_page"),
    path("transfer", views.transfer_page, name="transfer_page"),
    path("topup", views.topup_page, name="topup_page"),
]
