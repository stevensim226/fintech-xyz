from django.shortcuts import render, redirect


def welcome_page(request):
    return render(request, "welcome.html")


def login_page(request):
    return render(request, "login.html")


def logout_page(request):
    return render(request, "logout.html")


def register_page(request):
    return render(request, "register.html")


def inquiry_page(request):
    return render(request, "inquiry.html")


def create_payment_link_page(request):
    return render(request, "create_payment_link.html")


def payment_link_page(request, code):
    return render(request, "payment_link.html", {"code": code})


def transfer_page(request):
    return render(request, "transfer.html")


def topup_page(request):
    return render(request, "topup.html")
