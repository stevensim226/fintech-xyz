CREATE TABLE IF NOT EXISTS users (
   username VARCHAR(50) PRIMARY KEY,
   password VARCHAR(100),
   access_token VARCHAR(100) UNIQUE,
   refresh_token VARCHAR(100) UNIQUE,
   token_expiry TIMESTAMP
)