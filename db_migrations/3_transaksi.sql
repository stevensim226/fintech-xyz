CREATE TABLE IF NOT EXISTS transaksi (
   id_transaksi VARCHAR(50) PRIMARY KEY,
   tanggal TIMESTAMP,
   nominal INTEGER,
   status VARCHAR(50),
   tipe VARCHAR(50),
   keterangan VARCHAR(50),
   no_rekening_pengirim VARCHAR(10),
   no_rekening_penerima VARCHAR(10),
   FOREIGN KEY(no_rekening_pengirim) REFERENCES rekening(no_rekening),
   FOREIGN KEY(no_rekening_penerima) REFERENCES rekening(no_rekening)
)