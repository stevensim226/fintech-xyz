CREATE TABLE IF NOT EXISTS rekening (
   username VARCHAR(50) PRIMARY KEY,
   no_rekening VARCHAR(10) UNIQUE,
   saldo INTEGER,
   FOREIGN KEY(username) REFERENCES users(username)
)