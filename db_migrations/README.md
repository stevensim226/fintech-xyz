# Store all of your DB tables and alterations here with the name format `NUMBER_info.sql`

Migrate the tables using `psql -h ip_adress -U username -d DB_NAME -f /path/to/file.sql`