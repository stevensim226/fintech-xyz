from elasticsearch import Elasticsearch

INDEX_NAME = "fintechxyz-app"
ELASTIC_IP = "34.143.174.208"
ELASTIC_PORT = "9200"

LEVEL_WARNING = "WARNING"
LEVEL_ERROR = "ERROR"
LEVEL_INFO = "INFO"

es = Elasticsearch(f"http://{ELASTIC_IP}:{ELASTIC_PORT}")

def info(message, context):
	es.index(
		index=INDEX_NAME,
		document= {
			"level": LEVEL_INFO,
			"message": message,
			"context": context 
		}
	)

def warning(message, context):
	es.index(
		index=INDEX_NAME,
		document= {
			"level": LEVEL_WARNING,
			"message": message,
			"context": context 
		}
	)

def error(message, context):
	es.index(
		index=INDEX_NAME,
		document= {
			"level": LEVEL_ERROR,
			"message": message,
			"context": context 
		}
	)