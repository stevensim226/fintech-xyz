import os

DATABASE_HOST = os.environ.get("DB_HOST")
DATABASE_NAME = os.environ.get("DB_NAME")
DATABASE_USER = os.environ.get("DB_USER")
DATABASE_PASSWORD = os.environ.get("DB_PASS")
DATABASE_PORT = os.environ.get("DB_PORT")

AUTH_HOST = os.environ.get("AUTH_HOST")
AUTH_PORT = os.environ.get("AUTH_PORT")
AUTH_ROUTE = os.environ.get("AUTH_ROUTE")

RABBIT_MQ_HOST = os.environ.get("RABBIT_MQ_HOST")
RABBIT_MQ_PORT = os.environ.get("RABBIT_MQ_PORT")
RABBIT_MQ_USER = os.environ.get("RABBIT_MQ_USER")
RABBIT_MQ_PASSWORD = os.environ.get("RABBIT_MQ_PASSWORD")

DATABASE_URL = f"postgres://{DATABASE_USER}:{DATABASE_PASSWORD}@{DATABASE_HOST}:{DATABASE_PORT}/{DATABASE_NAME}"
AUTH_URL = f"http://{AUTH_HOST}:{AUTH_PORT}/{AUTH_ROUTE}"