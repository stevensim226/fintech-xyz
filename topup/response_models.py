from pydantic import BaseModel

class BaseResponse(BaseModel):
    success: bool

class TopupResponse(BaseResponse):
    message: str