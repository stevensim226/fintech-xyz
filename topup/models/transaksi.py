import os
import uuid

from datetime import datetime
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from exceptions import TopupTransactionInitiateException

from models import BASE, DB_SESSION
from common import xyz_logger

class Transaksi(BASE):
    __tablename__ = 'transaksi'

    id_transaksi = Column(String, primary_key=True)
    tanggal = Column(DateTime)
    nominal = Column(Integer)
    status = Column(String)
    tipe = Column(String)
    keterangan = Column(String)
    no_rekening_pengirim = Column(String, ForeignKey('rekening.no_rekening'))
    no_rekening_penerima = Column(String, ForeignKey('rekening.no_rekening'))

    @staticmethod
    def initiate_for_topup(nominal, no_rekening_topup):
        transaksi = Transaksi(id_transaksi=str(uuid.uuid4()), tanggal=datetime.now(), nominal=nominal,
                            status="ON GOING", tipe="debit", keterangan="TOPUP", no_rekening_pengirim=no_rekening_topup,
                            no_rekening_penerima=no_rekening_topup)

        try:
            DB_SESSION.add(transaksi)
            DB_SESSION.commit()
            xyz_logger.info(message=f"""
            Create On Going Transaction with Transaction ID {transaksi.id_transaksi}, Date {transaksi.tanggal}, Nominal {transaksi.nominal},
            Type {transaksi.tipe}, Description {transaksi.keterangan}, Sender Account {transaksi.no_rekening_pengirim}, Target Account {transaksi.no_rekening_penerima}
            """, context="topup"
            )
        except Exception:
            xyz_logger.error(message=f"""
            Fail to Create On Going Transaction with Transaction ID {transaksi.id_transaksi}, Date {transaksi.tanggal}, Nominal {transaksi.nominal},
            Type {transaksi.tipe}, Description {transaksi.keterangan}, Sender Account {transaksi.no_rekening_pengirim}, Target Account {transaksi.no_rekening_penerima}
            """, context="topup"
            )
            raise TopupTransactionInitiateException("Something's gone wrong when initiating topup transaction")

        return transaksi