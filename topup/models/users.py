import os

from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from exceptions import RekeningNotFoundException

from models import BASE, DB_SESSION

class Users(BASE):
    __tablename__ = 'users'

    username = Column(String, primary_key=True)
    password = Column(String)
    access_token = Column(String, unique=True)
    refresh_token = Column(String, unique=True)
    token_expiry = Column(DateTime)