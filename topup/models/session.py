from sqlalchemy.engine import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from config import DATABASE_URL

DB_SESSION = scoped_session(sessionmaker(autocommit=False, autoflush=False))
BASE = declarative_base()
BASE.query = DB_SESSION.query_property()

connection_str = DATABASE_URL
engine = create_engine(connection_str)
DB_SESSION.configure(bind=engine)