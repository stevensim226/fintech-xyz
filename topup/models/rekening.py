import os

from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from exceptions import RekeningNotFoundException

from models import BASE, DB_SESSION

class Rekening(BASE):
    __tablename__ = 'rekening'

    username = Column(String, ForeignKey('users.username'), primary_key=True)
    users = relationship("Users", foreign_keys=[username])
    no_rekening = Column(String)
    saldo = Column(Integer)

    @staticmethod
    def topup(username, topup_amount):
        rekening = Rekening.query.filter_by(username=username).first()
        
        try:
            rekening.saldo += topup_amount
            DB_SESSION.commit()
        except AttributeError:
            raise RekeningNotFoundException("Rekening Not Found")

        return rekening

    @staticmethod
    def get_rekening_from_username(username):
        rekening = Rekening.query.filter_by(username=username).first()

        if rekening:
            return rekening
        else:
            raise RekeningNotFoundException("Rekening Not Found")