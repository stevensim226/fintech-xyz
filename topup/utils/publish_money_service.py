import pika
import json

from config import RABBIT_MQ_HOST, RABBIT_MQ_PORT, RABBIT_MQ_USER, RABBIT_MQ_PASSWORD
from exceptions import TopupTransactionPublishException
from common import xyz_logger

def publish_topup(id_transaksi, no_rekening, amount):
    # Declare broadcast setup for RabbitMQ
    TOPUP_EXCHANGE = "topup"
    TOPUP_QUEUE = "topup_queue"
    credentials = pika.PlainCredentials(RABBIT_MQ_USER, RABBIT_MQ_PASSWORD)
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(RABBIT_MQ_HOST, int(RABBIT_MQ_PORT), credentials=credentials))
    channel = connection.channel()
    
    # format input as json
    details_json = {
        "transfer_id": id_transaksi,
        "no_rekening": no_rekening,
        "amount": amount,
    }

    # format json as string
    formatted_json = json.dumps(details_json)

    # create exchange (hubs before queues)
    channel.exchange_declare(exchange=TOPUP_EXCHANGE,
                            exchange_type="direct",
                            durable=True)

    # queue declaration
    channel.queue_declare(queue=TOPUP_QUEUE, durable=True)

    # bind queues to exchange
    channel.queue_bind(exchange=TOPUP_EXCHANGE,
                    queue=TOPUP_QUEUE,
                    routing_key=TOPUP_EXCHANGE)

    try:
        # publish
        channel.basic_publish(
            exchange=TOPUP_EXCHANGE,
            routing_key=TOPUP_EXCHANGE,
            body=formatted_json,
            properties=pika.BasicProperties()
        )
        xyz_logger.info(message=f"Published Topup Transaction for No Rekening {no_rekening}, Transaction ID {id_transaksi}, and Total Amount {amount}", context="topup")
    except Exception:
        xyz_logger.error(message=f"Failed to Publish Topup Transaction for No Rekening {no_rekening}, Transaction ID {id_transaksi}, and Total Amount {amount}", context="topup")
        raise TopupTransactionPublishException("Something's gone wrong when publishing topup transaction")