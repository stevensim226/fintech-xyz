from pydantic import BaseModel

class TopupRequest(BaseModel):
    amount: int