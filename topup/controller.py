import requests

from fastapi import FastAPI, Header
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware

from config import AUTH_URL
from models import Rekening, Transaksi
from response_models import TopupResponse
from request_models import TopupRequest

from exceptions import RekeningNotFoundException, TopupTransactionInitiateException, TopupTransactionPublishException
from utils import publish_topup

AUTH_HEADER = Header(None, description="Bearer token that contains the access token. Must be in 'Bearer ...' format")

app = FastAPI()

app.add_middleware(CORSMiddleware, 
    allow_origins = ["*"],
    allow_methods=["*"],
    allow_headers=["*"]
)

def authenticate(access_token):
    headers = {'authentication': access_token}
    response = requests.get(AUTH_URL, headers=headers)

    if response.status_code == 401:
        raise ValueError("Token not valid")
    
    return response

@app.post("/topup", 
        response_model=TopupResponse, 
        responses={
            500: {"model": TopupResponse},
            401: {"model": TopupResponse},
            404: {"model": TopupResponse}
        })
async def topup(request: TopupRequest, authentication=AUTH_HEADER):
    whoami = authenticate(authentication)
    if whoami.status_code == 401:
        return JSONResponse(status_code=401, content={
            "success": False,
            "message": "Token not valid!"
        })
    else:
        whoami = whoami.json()
    
    try:
        rekening = Rekening.get_rekening_from_username(username = whoami['username'])
        transaksi = Transaksi.initiate_for_topup(nominal = request.amount, no_rekening_topup = rekening.no_rekening)
        publish_topup(id_transaksi = transaksi.id_transaksi, no_rekening = rekening.no_rekening, amount = request.amount)   
    except RekeningNotFoundException:
        return JSONResponse(status_code=404, content={
            "success": False,
            "message": "Rekening Not Found"
        })
    except TopupTransactionInitiateException:
        return JSONResponse(status_code=500, content={
            "success": False,
            "message": "Topup Transaction Initiation Gone Wrong. Please Try Again Later"
        })
    except TopupTransactionPublishException:
        return JSONResponse(status_code=500, content={
            "success": False,
            "message": "Topup Transaction Publish Gone Wrong. Please Try Again Later"
        })

    return JSONResponse(status_code=200, content={
        "success": True,
        "message": "Topup Transaction Initiated. Check Inquiries for further details"
    })